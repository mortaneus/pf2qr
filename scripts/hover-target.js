export function hoverTarget() {
    $("#chat-log").on('mouseover', '.targetPicker', function () {
        $(this).css("background-color", "yellow");
        let target = (canvas.tokens.placeables.find(t => t.data._id === $(this).attr("data-target")));
        console.log(target);
    });
    $("#chat-log").on('dblclick', '.targetPicker', function (e) {
        let base = this;
        if (!e.shiftKey) { (canvas.tokens.placeables.find(t => t.data._id === $(this).attr("data-target"))).control({ releaseOthers: true }); }
        $(this).parent().children(".targetPicker").each(function () {
            if ($(base).attr("data-hitType") === $(this).attr("data-hitType")) {
                $(this).finish().fadeOut(40).fadeIn(40);
				(canvas.tokens.placeables.find(t => t.data._id === $(this).attr("data-target"))).control({ releaseOthers: false }); 
            }
        });
    })
    $("#chat-log").on('click', '.targetPicker', function (e) {
        $(this).finish().fadeOut(40).fadeIn(40);
        if (e.shiftKey) {
            if ((canvas.tokens.placeables.find(t => t.data._id === $(this).attr("data-target")))._controlled) {
				(canvas.tokens.placeables.find(t => t.data._id === $(this).attr("data-target"))).release({ releaseOthers: false }); 
            } else {
				(canvas.tokens.placeables.find(t => t.data._id === $(this).attr("data-target"))).control({ releaseOthers: false }); 
            }
        } else {
            if ((canvas.tokens.placeables.find(t => t.data._id === $(this).attr("data-target")))._controlled) {
                if (game.user.targets.size > 1) {
					(canvas.tokens.placeables.find(t => t.data._id === $(this).attr("data-target"))).control({ releaseOthers: true }); 	
                } else {
					(canvas.tokens.placeables.find(t => t.data._id === $(this).attr("data-target"))).release({ releaseOthers: true }); 	
                }
            } else {
				(canvas.tokens.placeables.find(t => t.data._id === $(this).attr("data-target"))).control({ releaseOthers: true }); 	
            }
        }
    });
    $("#chat-log").on('mouseout', '.targetPicker', function () {
        $(this).css("background-color", "transparent");
    });
}
