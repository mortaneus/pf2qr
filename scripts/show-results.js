export function showResults(chatData) {
    //Use this to determine if it should be shown to players or not.
    if (!game.settings.get("pf2qr", "ShowPlayersResults")) {
        chatData.user = game.users.entities.find(u => u.isGM)._id; //Imitating GM so that we don't see our own message to the GM, in the case it is a player rolling.
        chatData.speaker = ChatMessage.getSpeaker({ user: game.user });
		
		if (!game.user.isGM) {
			// Socket decisions. If user is not a Gm and showresults is false, send data to GM.
			console.log("EMITTING USER DATA TO GM")
			game.socket.emit('module.pf2qr', chatData);
		} else {
			// If GM and showResults is false, process chatmessage as normal and whisper to GM
			chatData["whisper"] = ChatMessage.getWhisperRecipients("GM");
			console.log("PROCESSING USER DATA AS GM!")
			ChatMessage.create(chatData);
		}
    } else {
		// Send details to everyone
        ChatMessage.create(chatData);
    }
}